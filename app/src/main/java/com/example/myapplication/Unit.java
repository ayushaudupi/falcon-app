package com.example.myapplication;

public class Unit {

    String name;
    String image;
    String phone;

    public Unit() {
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getPhone() {
        return phone;
    }
}
