package com.example.myapplication;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.UnitViewHolder> implements Filterable {

    private List<UnitItem> exampleList;
    private List<UnitItem> exampleListFull;

    class UnitViewHolder extends RecyclerView.ViewHolder {
        TextView step_name,status,activity_name,progress,textView1;

        UnitViewHolder(View itemView) {
            super(itemView);
            step_name = (TextView) itemView.findViewById(R.id.step_name);
            status = (TextView) itemView.findViewById(R.id.status);
            activity_name = (TextView) itemView.findViewById(R.id.activity_name);
            progress = (TextView) itemView.findViewById(R.id.progress);
//            textView1 = (TextView) itemView.findViewById(R.id.textView1);
        }
    }
    UnitAdapter(List<UnitItem> exampleList) {
        this.exampleList = exampleList;
        exampleListFull = new ArrayList<>(exampleList);
    }
    @NonNull
    @Override
    public UnitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.unit_item, parent, false);
        return new UnitViewHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull UnitViewHolder holder, int position) {
        UnitItem currentItem = exampleList.get(position);
//        holder.textView1.setText(currentItem.getText1());
        holder.activity_name.setText(currentItem.getActivityname());
        holder.status.setText(currentItem.getActivitystatus());
        holder.step_name.setText(currentItem.getStepname());
        holder.progress.setText(currentItem.getProgress() + '%');
    }
    @Override
    public int getItemCount() {
        return exampleList.size();
    }
    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<UnitItem> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(exampleListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (UnitItem item : exampleListFull) {
                    if (item.getActivityname().toLowerCase().contains(filterPattern) || item.getStepname().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            exampleList.clear();
            exampleList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

}
